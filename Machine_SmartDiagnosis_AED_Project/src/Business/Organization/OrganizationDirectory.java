/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Organization.Organization.Type;
import java.util.ArrayList;


public class OrganizationDirectory {
    
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Type type){
        Organization organization = null;
        if (type.getValue().equals(Type.HospitalAdminOrganization.getValue())){
            organization = new HospitalAdminOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.AutomobileAdminOrganization.getValue())){
            organization = new AutomobileAdminOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.Technician.getValue())){
            organization = new TechnicianOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.Specialist.getValue())){
            organization = new SpecialistOrganization();
            organizationList.add(organization);
        }
        return organization;
    }
}