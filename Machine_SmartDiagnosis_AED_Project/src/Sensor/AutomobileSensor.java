/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sensor;
import Business.Enterprise.Enterprise;
import java.util.Random;


/**
 *
 * @author shweta
 */
public class AutomobileSensor {
   static Random random = new Random();
   public AutomobileSensorData automobileSensorData(Enterprise enterprise)
   {
       
       AutomobileSensorData automobileDetection=enterprise.getMachineryDetectionDirectory().addAutomobileDetection();
       automobileDetection.setAirLockCheck(random.nextBoolean());
       automobileDetection.setBrakeLines((random.nextInt(4)));
       automobileDetection.setEmission((random.nextInt(7-4+1)));
       automobileDetection.setOilPressure(Double.parseDouble(String.format("%.1f",(random.nextDouble()*1)+1)));
       automobileDetection.setHydraulicPressure(Double.parseDouble(String.format("%.1f",random.nextDouble()*2)));
       return automobileDetection;
   }
   
   
   
}
