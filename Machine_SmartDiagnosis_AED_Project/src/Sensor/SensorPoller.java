/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sensor;

import Business.Core.INotifier;
import java.sql.Time;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledFuture;
import static java.util.concurrent.TimeUnit.SECONDS;
import sun.management.Sensor;

/**
 *
 * @author shweta
 */
public class SensorPoller extends Timer {

    public SensorPoller(int pollingInterval) {

        this.pollingInterval = pollingInterval * 4000;
    }

    public void subscribe(INotifier notifier) {
        this.notifier = notifier;
    }

    public void start() {

        this.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                SensorPoller.this.notifier.publish();
            }
        }, 0, pollingInterval);
    }
    public void Stop(){
        this.cancel();
    }
    private int pollingInterval;
    private INotifier notifier;
}
