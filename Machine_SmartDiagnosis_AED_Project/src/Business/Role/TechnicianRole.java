/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.AdministrativeRole.AdminWorkAreaJPanel;
import javax.swing.JPanel;
import userinterface.TechnicianRole.TechnicianWorkAreaJPanel;


public class TechnicianRole extends Role{

    public TechnicianRole() {
        super(RoleType.Technician.getValue());
    }

    
    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new TechnicianWorkAreaJPanel(userProcessContainer, account,organization,business);
    }

    
    
}
