/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.SpecialistRole;

import Business.Core.INotifier;
import Business.EcoSystem;
import Business.Enterprise.AutomobileEnterprise;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.Organization.SpecialistOrganization;
import Business.Organization.TechnicianOrganization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.AutomobileSensorDataWorkRequest;
import Business.WorkQueue.WorkRequest;
import Sensor.AutomobileSensorData;
import Sensor.AutomobileSensor;
import Sensor.AutomobileSensorDataAnalyzer;
import Sensor.MachineryDetectionDirectory;
import Sensor.SensorPoller;
import Sensor.SensorStatus;
import Sensor.SensorTroubleShootDirectory;
import Sensor.workRequest.AutomobileSensorTroubleShootWorkRequest;
import java.awt.CardLayout;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import static java.util.concurrent.TimeUnit.SECONDS;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

public class SpecialistWorkAreaJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private SpecialistOrganization organization;
    private Enterprise enterprise;
    private UserAccount userAccount;
    private AutomobileSensor sensor;
    private AutomobileSensorData automobileSensorData;
    private SensorPoller sensorPoller;
    private AutomobileSensorDataAnalyzer automobileSensorDataAnalyzer;
    private AutomobileEnterprise automobileEnterprise;
  
    private SensorTroubleShootDirectory sensorTroubleShootDirectory;
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    DefaultTableModel model1;
    /**
     * Creates new form SpecialistWorkAreaJPanel
     */
    public SpecialistWorkAreaJPanel(JPanel userProcessContainer, UserAccount account, SpecialistOrganization organization, Enterprise enterprise) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.organization = organization;
        this.enterprise = enterprise;
        this.userAccount = account;
        this.automobileSensorDataAnalyzer=new AutomobileSensorDataAnalyzer();
        sensor = new AutomobileSensor();
        sensorPoller = new SensorPoller(2);
        this.automobileEnterprise=(AutomobileEnterprise)enterprise;
        this.sensorTroubleShootDirectory=enterprise.getSensorTroubleShootDirectory();
       
        sensorPoller.subscribe(new INotifier() {

            @Override
            public void publish() {
                AutomobileSensorData sensorData = sensor.automobileSensorData(enterprise);
               SensorStatus sensorStatus= automobileSensorDataAnalyzer.analyze(sensorData);
               populateSensorData(sensorData,sensorStatus);
                
             }
        });
        sensorPoller.start();
        valueLabel.setText(enterprise.getName());
        
          model1 = new DefaultTableModel(){
          @Override
          public Class<?> getColumnClass(int column){
              switch(column){
                  case 2: return ImageIcon.class;
                  default:return Object.class;
              }
          } 
       };
        DiagnosticTaskJTable.setModel(model1);
       model1.setColumnCount(0);
       model1.addColumn("Message");
       model1.addColumn("Status");
       model1.addColumn("Result");
    }

    /**
     * Creates new form SpecialistWorkAreaJPanel
     */
    public void populateRequestTable(SensorStatus sensorStatus) {
        System.out.println("populateRequestTable-----"+userAccount.getWorkQueue().getWorkRequestList().size());
        DefaultTableModel model = (DefaultTableModel) workRequestJTable.getModel();
        model.setRowCount(0);
        for (WorkRequest request : userAccount.getWorkQueue().getWorkRequestList()) {
            Object[] row = new Object[4];
            row[0] = request;
            row[1] = request.getReceiver();
            row[2] = request.getStatus();
            String result = ((AutomobileSensorDataWorkRequest) request).getTestResult();
            row[3] = result == null ? "Waiting" : result;
            model.addRow(row);
            
        }
    }

    public void populateSensorData(AutomobileSensorData automobileSensorData, SensorStatus sensorStatus) {
        DefaultTableModel model = (DefaultTableModel) SpecialistJTable.getModel();
        String a="";
        String b="";
       try{
       
        Object[] row = new Object[6];
        row[0] = automobileSensorData;
        row[1] = automobileSensorData.getBrakeLines();
        row[2] = automobileSensorData.getHydraulicPressure();
        row[3] = automobileSensorData.getEmission();
        row[4] = automobileSensorData.getAirLockCheck();
        row[5] = sensorStatus;
        model.addRow(row);
        
        if(automobileSensorData.getEmission()>=3.0){
            a+="Emmission, ";
        }
        if(automobileSensorData.getHydraulicPressure()>=2.0){
            a+="Hydraulic Pressure, ";
        }
        if(automobileSensorData.getBrakeLines()==3){
            a+="BrakeLines, ";
        }
        
            
        
        if(sensorStatus==SensorStatus.Critical){    
           AutomobileSensorDataWorkRequest request = new AutomobileSensorDataWorkRequest();
        request.setMessage(a);
        request.setSender(userAccount);
        request.setStatus("Sent");
       
        Organization org = null;
        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
            if (organization instanceof TechnicianOrganization){
                org = organization;
                break;
            }
        }
        if (org!=null){
            System.out.println("add request");
            org.getWorkQueue().getWorkRequestList().add(request);
            userAccount.getWorkQueue().getWorkRequestList().add(request);
        }
            System.out.println("before");
          populateRequestTable(sensorStatus);  
        }
        if(automobileSensorData.getEmission()>=2.0&& automobileSensorData.getEmission()<3.0){
            b+="Emmission, ";
        }
        if(automobileSensorData.getHydraulicPressure()<2.0&& automobileSensorData.getHydraulicPressure()>=1.0){
            b+="Hydraulic Pressure, ";
        }
        if(automobileSensorData.getBrakeLines()==2){
            b+="BrakeLines, ";
        }
       if(sensorStatus==SensorStatus.TroubleShoot){
           AutomobileSensorTroubleShootWorkRequest request1 = new AutomobileSensorTroubleShootWorkRequest();
       
           request1.setMessage(b);
           request1.setStatus("TroubleShooting..");
           populateSensorAutoDiagnosisResult(sensorStatus,request1, false);
          
       }
    }
       
       catch(Exception e){
            e.printStackTrace();
        }
    }
   
   public void populateSensorAutoDiagnosisResult(SensorStatus sensorStatus,AutomobileSensorTroubleShootWorkRequest automobileSensorTroubleShootWorkRequest, boolean flag){
       
        DefaultTableModel model = (DefaultTableModel) DiagnosticTaskJTable.getModel();
        
        try{
         {  
            sensorTroubleShootDirectory.addAutomobileTroubleShootWorkRequest(automobileSensorTroubleShootWorkRequest);
            Object[] row = new Object[3];
            row[0] = automobileSensorTroubleShootWorkRequest.getMessage();
            row[1] = automobileSensorTroubleShootWorkRequest.getStatus();

            if(automobileSensorTroubleShootWorkRequest.getStatus().equals("Done")){
                row[2]= new ImageIcon(getClass().getResource("/Image/tick.gif"));
           }else{
                row[2]="";
            }            
            model.addRow(row);
            if(flag == false){
                Timer timer = new Timer(); 
            TimerTask action = new TimerTask() {
                   
               @Override
               public void run() {
                  automobileSensorTroubleShootWorkRequest.setStatus("Done");
                  automobileSensorTroubleShootWorkRequest.setTestResult("Set Result");
                               model.removeRow(model.getRowCount()-1);
                 
                   populateSensorAutoDiagnosisResult(sensorStatus, automobileSensorTroubleShootWorkRequest, true);
                   
               }
               
           };
            timer.schedule(action,3000);
            
            }         
           
        }
       
        }
         catch(Exception e){
                e.printStackTrace();
                }
   }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.x`
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        workRequestJTable = new javax.swing.JTable();
        enterpriseLabel = new javax.swing.JLabel();
        valueLabel = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        SpecialistJTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jCriticalStatus = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        DiagnosticTaskJTable = new javax.swing.JTable();
        jCriticalStatus1 = new javax.swing.JLabel();
        StartjButton = new javax.swing.JButton();
        ShutdownjButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        workRequestJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Parameters", "Receiver", "Technician", "Result"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                true, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(workRequestJTable);
        if (workRequestJTable.getColumnModel().getColumnCount() > 0) {
            workRequestJTable.getColumnModel().getColumn(1).setResizable(false);
            workRequestJTable.getColumnModel().getColumn(2).setResizable(false);
            workRequestJTable.getColumnModel().getColumn(3).setResizable(false);
        }

        enterpriseLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        enterpriseLabel.setText("EnterPrise Name :");

        valueLabel.setText("<value>");

        SpecialistJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Oil Pressure", "Brake Lines", "Hydraulic Pressure", "Emission", "Air Lock Check", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, true, true, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(SpecialistJTable);

        jLabel1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        jLabel1.setText("AutoMobile Sensor Data ");

        jCriticalStatus.setFont(new java.awt.Font("Tahoma", 2, 14)); // NOI18N
        jCriticalStatus.setText("Critical Tasks ");

        DiagnosticTaskJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Message", "Status", "Result"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(DiagnosticTaskJTable);
        if (DiagnosticTaskJTable.getColumnModel().getColumnCount() > 0) {
            DiagnosticTaskJTable.getColumnModel().getColumn(0).setResizable(false);
            DiagnosticTaskJTable.getColumnModel().getColumn(1).setResizable(false);
            DiagnosticTaskJTable.getColumnModel().getColumn(2).setResizable(false);
        }

        jCriticalStatus1.setFont(new java.awt.Font("Tahoma", 2, 14)); // NOI18N
        jCriticalStatus1.setText("Self DiagnosticTasks ");

        StartjButton.setText("Sensor Start");
        StartjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                StartjButtonActionPerformed(evt);
            }
        });

        ShutdownjButton.setText("Sensor Shutdown");
        ShutdownjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ShutdownjButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jCriticalStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 559, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jCriticalStatus1, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 719, Short.MAX_VALUE)
                                .addComponent(enterpriseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(valueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(29, 29, 29)))
                        .addGap(23, 23, 23))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(StartjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(ShutdownjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(97, 97, 97))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(enterpriseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(valueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(StartjButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ShutdownjButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCriticalStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCriticalStatus1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(92, 92, 92))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void StartjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_StartjButtonActionPerformed
        // TODO add your handling code here:
       
        sensorPoller = new SensorPoller(2);
        sensorPoller.subscribe(new INotifier() {

            @Override
            public void publish() {
                AutomobileSensorData sensorData = sensor.automobileSensorData(enterprise);
               SensorStatus sensorStatus= automobileSensorDataAnalyzer.analyze(sensorData);
                populateSensorData(sensorData,sensorStatus);
                
             }
        });
        sensorPoller.start();
    }//GEN-LAST:event_StartjButtonActionPerformed

    private void ShutdownjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ShutdownjButtonActionPerformed
        // TODO add your handling code here:
        sensorPoller.Stop();
    }//GEN-LAST:event_ShutdownjButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable DiagnosticTaskJTable;
    private javax.swing.JButton ShutdownjButton;
    private javax.swing.JTable SpecialistJTable;
    private javax.swing.JButton StartjButton;
    private javax.swing.JLabel enterpriseLabel;
    private javax.swing.JLabel jCriticalStatus;
    private javax.swing.JLabel jCriticalStatus1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel valueLabel;
    private javax.swing.JTable workRequestJTable;
    // End of variables declaration//GEN-END:variables
}
