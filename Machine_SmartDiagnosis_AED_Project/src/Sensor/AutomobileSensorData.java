/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sensor;



/**
 *
 * @author shweta
 */
public class AutomobileSensorData {
    private double oilPressure;
    private boolean airLockCheck;
    private double bodyVibration;
    private double hydraulicPressure;
    private int brakeLines;
    private double emissionRate;

   public AutomobileSensorData(){
       
   }
    public double getOilPressure() {
        return oilPressure;
    }

    public void setOilPressure(double oilPressure) {
        this.oilPressure = oilPressure;
    }

    public boolean getAirLockCheck() {
        return airLockCheck;
    }

    public void setAirLockCheck(boolean airLockCheck) {
        this.airLockCheck = airLockCheck;
    }

    public double getBodyVibration() {
        return bodyVibration;
    }

    public void setBodyVibration(double bodyVibration) {
        this.bodyVibration = bodyVibration;
    }

    public double getHydraulicPressure() {
        return hydraulicPressure;
    }

    public void setHydraulicPressure(double hydraulicPressure) {
        this.hydraulicPressure = hydraulicPressure;
    }

    public int getBrakeLines() {
        return brakeLines;
    }

    public void setBrakeLines(int brakeLines) {
        this.brakeLines = brakeLines;
    }

    public double getEmission() {
        return emissionRate;
    }

    public void setEmission(double emission) {
        this.emissionRate = emission;
    }

    @Override
    public String toString() {
        return String.valueOf(oilPressure);
    }
    
}
