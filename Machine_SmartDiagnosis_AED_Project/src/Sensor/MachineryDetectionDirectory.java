/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sensor;


import java.util.ArrayList;

/**
 *
 * @author shweta
 */
public class MachineryDetectionDirectory {
    private ArrayList<AutomobileSensorData> automobileDetectionList;

    public MachineryDetectionDirectory() {
        this.automobileDetectionList = new ArrayList<>();
    }

    public ArrayList<AutomobileSensorData> getAutomobileDetectionList() {
        return automobileDetectionList;
    }
    
    public AutomobileSensorData addAutomobileDetection(){
        AutomobileSensorData auto = new AutomobileSensorData();
        automobileDetectionList.add(auto);
        return auto;
    }
    
}
