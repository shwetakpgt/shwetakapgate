/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;


public abstract class Role {
    
    private String name;
    public enum RoleType{
        HospitalAdminRole("Hospital Admin Role"),
        AutomobileAdminRole("Automobile Admin Role"),
        Technician("Technician"),
        Specialist("Specialist"),
        SystemAdmin("System Admin");
        private String value;
        private RoleType(String value){
            this.value = value;
        }
        public String getValue() {
            return value;
        }
        @Override
        public String toString() {
            return value;
        }
    }
    
    public Role(String name){
        this.name=name;
    }
    public abstract JPanel createWorkArea(JPanel userProcessContainer, 
            UserAccount account, 
            Organization organization, 
            Enterprise enterprise, 
            EcoSystem business);

    @Override
    public String toString() {
         return name;
    }
    
    
}