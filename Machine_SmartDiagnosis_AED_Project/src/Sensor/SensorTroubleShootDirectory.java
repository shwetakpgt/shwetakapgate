/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sensor;

import Sensor.workRequest.AutomobileSensorTroubleShootWorkRequest;
import java.util.ArrayList;

/**
 *
 * @author shweta
 */
public class SensorTroubleShootDirectory {

    public SensorTroubleShootDirectory() {
        workRequestList = new ArrayList<>();
    }

    
     private ArrayList<AutomobileSensorTroubleShootWorkRequest> workRequestList;

    public ArrayList<AutomobileSensorTroubleShootWorkRequest> getWorkRequestList() {
        return workRequestList;
    }

    public void setWorkRequestList(ArrayList<AutomobileSensorTroubleShootWorkRequest> workRequestList) {
        this.workRequestList = workRequestList;
    }
     
     public AutomobileSensorTroubleShootWorkRequest addAutomobileTroubleShootWorkRequest(AutomobileSensorTroubleShootWorkRequest auto){
        workRequestList.add(auto);
        return auto;
    }
}
