﻿4:29 PM 4/27/2016-- Table: "School"."dimAbsenceCode"

-- DROP TABLE "School"."dimAbsenceCode";

CREATE TABLE "School"."dimAbsenceCode"
(
  "AbsenceCodeID" integer NOT NULL,
  "AbsenceCode" character(1),
  "AbsenceCodeDesc" character(50),
  "Unexcused" character(20),
  CONSTRAINT absencecode_pk PRIMARY KEY ("AbsenceCodeID")
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "School"."dimAbsenceCode"
  OWNER TO postgres;

copy "School"."dimAbsenceCode" from "C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/dimAbsenceCode.txt" WITH DELIMITER "," CSV HEADER QUOTE ''''

CREATE TABLE "School"."dimAssignment"(
	"AssignmentID" integer NOT NULL,
	"Assignment" character(200) NULL,
 CONSTRAINT "dimAssignment_PK" PRIMARY KEY ("AssignmentID") 
)

copy "School"."dimAssignment" from "C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/dimAssignment.txt" WITH DELIMITER "," CSV HEADER QUOTE ''''

select * from "School"."dimAssignment"


CREATE TABLE "School"."dimCourse"(
	"CourseID" integer NOT NULL,
	"SubjectID" integer NULL,
	"CourseSISID" character(50) NULL,
	"CourseTitle" character(50) NULL,
	"CourseLevel" character(50) NULL,
 CONSTRAINT "dimCourse_PK" PRIMARY KEY ("CourseID") 
)

copy "School"."dimCourse" from "C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/dimCourse.txt" WITH DELIMITER "," CSV HEADER QUOTE ''''

select * from "School"."dimCourse"

CREATE TABLE "School"."dimDate"(
	"DatePK" date NOT NULL,
	"DateName" character(50) NULL,
	"Year" date NULL,
	"YearName" character(50) NULL,
	"HalfYear" date NULL,
	"HalfYearName" character(50) NULL,
	"Quarter" date NULL,
	"QuarterName" character(50) NULL,
	"Month" date NULL,
	"MonthName" character(50) NULL,
	"Week" date NULL,
	"WeekName" character(50) NULL,
	"DayOfYear" integer NULL,
	"DayOfYearName" character(50) NULL,
	"DayOfHalfYear" integer NULL,
	"DayOfHalfYearName" character(50) NULL,
	"DayOfQuarter" integer NULL,
	"DayOfQuarterName" character(50) NULL,
	"DayOfMonth" integer NULL,
	"DayOfMonthName" character(50) NULL,
	"DayOfWeek" integer NULL,
	"DayOfWeekName" character(50) NULL,
	"WeekOfYear" integer NULL,
	"WeekOfYearName" character(50) NULL,
	"MonthOfYear" integer NULL,
	"MonthOfYearName" character(50) NULL,
	"MonthOfHalfYear" integer NULL,
	"MonthOfHalfYearName" character(50) NULL,
	"MonthOfQuarter" integer NULL,
	"MonthOfQuarterName" character(50) NULL,
	"QuarterOfYear" integer NULL,
	"QuarterOfYearName" character(50) NULL,
	"QuarterOfHalfYear" integer NULL,
	"QuarterOfHalfYearName" character(50) NULL,
	"HalfYearOfYear" integer NULL,
	"HalfYearOfYearName" character(50) NULL,
	"SchoolYear" date NULL,
	"SchoolYearName" character(50) NULL,
	"SchoolHalfYear" date NULL,
	"SchoolHalfYearName" character(50) NULL,
	"SchoolQuarter" date NULL,
	"SchoolQuarterName" character(50) NULL,
	"SchoolMonth" date NULL,
	"SchoolMonthName" character(50) NULL,
	"SchoolWeek" date NULL,
	"SchoolWeekName" character(50) NULL,
	"SchoolDate" date NULL,
	"SchoolDayName" character(50) NULL,
	"SchoolDayOfYear" integer NULL,
	"SchoolDayOfYearName" character(50) NULL,
	"SchoolDayOfHalfYear" integer NULL,
	"SchoolDayOfHalfYearName" character(50) NULL,
	"SchoolDayOfQuarter" integer NULL,
	"SchoolDayOfQuarterName" character(50) NULL,
	"SchoolDayOfMonth" integer NULL,
	"SchoolDayOfMonthName" character(50) NULL,
	"SchoolDayOfWeek" integer NULL,
	"SchoolDayOfWeekName" character(50) NULL,
	"SchoolWeekOfYear" integer NULL,
	"SchoolWeekOfYearName" character(50) NULL,
	"SchoolMonthOfYear" integer NULL,
	"SchoolMonthOfYearName" character(50) NULL,
	"SchoolMonthOfHalfYear" integer NULL,
	"SchoolMonthOfHalfYearName" character(50) NULL,
	"SchoolMonthOfQuarter" integer NULL,
	"SchoolMonthOfQuarterName" character(50) NULL,
	"SchoolQuarterOfYear" integer NULL,
	"SchoolQuarterOfYearName" character(50) NULL,
	"SchoolQuarterOfHalfYear" integer NULL,
	"SchoolQuarterOfHalfYearName" character(50) NULL,
	"SchoolHalfYearOfYear" integer NULL,
	"SchoolHalfYearOfYearName" character(50) NULL,
	"SchoolDay" integer NULL,
	"SchoolNineWeeks" integer NULL,
 CONSTRAINT "dimDate_PK" PRIMARY KEY  ("DatePK")
)

copy "School"."dimDate" from "C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/dimDate.txt" WITH DELIMITER "," CSV HEADER QUOTE ''''

select * from "School"."dimDate"

CREATE TABLE "School"."dimDisciplineActionCode"(
	"DisciplineActionCodeID" integer NOT NULL,
	"DisciplineActionCode" character(10) NULL,
	"DisciplineActionDesc" character(50) NULL,
 CONSTRAINT "dimDisciplineActionCode_PK" PRIMARY KEY ("DisciplineActionCodeID"))

 copy "School"."dimDisciplineActionCode" from "C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/dimDisciplineActionCode.txt" WITH DELIMITER "," CSV HEADER QUOTE ''''

select * from "School"."dimDisciplineActionCode"

CREATE TABLE "School"."dimDisciplineEventCode"(
	"DisciplineEventCodeID" integer NOT NULL,
	"DisciplineEventCode" character(10) NULL,
	"DisciplineEventDesc" character(50) NULL,
 CONSTRAINT "dimDisciplineEventCode_PK" PRIMARY KEY ("DisciplineEventCodeID"))

  copy "School"."dimDisciplineEventCode" from "C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/dimDisciplineEventCode.txt" WITH DELIMITER "," CSV HEADER QUOTE ''''

select * from "School"."dimDisciplineEventCode"


 CREATE TABLE "School"."dimEnrollmentCode"(
	"EnrollmentCodeID" integer NOT NULL,
	"EnrollmentCode" character(10) NULL,
	"EnrollmentCodeDesc" character(50) NULL,
 CONSTRAINT "dimEnrollmentCode_PK" PRIMARY KEY ("EnrollmentCodeID"))

  copy "School"."dimEnrollmentCode" from "C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/dimEnrollmentCode.txt" WITH DELIMITER "," CSV HEADER QUOTE ''''

select * from "School"."dimEnrollmentCode"

CREATE TABLE "School"."dimFacility"(
	"FacilityID" integer NOT NULL,
	"Facility" character(100) NULL,
	"Address" character(50) NULL,
	"City" character(50) NULL,
	"Zip" character(20) NULL,
	"Active" character(20) NULL,
	"Charter" character(10) NULL,
	"FacilityADCode" character(50) NULL,
	"FacLevel" character(50) NULL,
	"FacilityName" character(50) NULL,
	"FacilityType" character(50) NULL,
 CONSTRAINT "dimFacility_PK" PRIMARY KEY ("FacilityID")) 

 copy "School"."dimFacility" from "C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/dimFacility.txt" WITH DELIMITER "," CSV HEADER QUOTE ''''

select * from "School"."dimFacility"

CREATE TABLE "School"."dimGradeType"(
	"GradeTypeID" integer NOT NULL,
	"GradeType" character(50) NULL,
 CONSTRAINT "dimGradeType_PK" PRIMARY KEY ("GradeTypeID")) 

 copy "School"."dimGradeType" from "C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/dimGradeType.txt" WITH DELIMITER "," CSV HEADER QUOTE ''''

select * from "School"."dimGradeType"

CREATE TABLE "School"."dimHSGTDomain"(
	"HSGTDomainID" integer NOT NULL,
	"HSGTDomain" character(200) NULL,
	"SubjectID" integer NULL,
 CONSTRAINT "PK_dimHSGTDomain" PRIMARY KEY ("HSGTDomainID"))  

 copy "School"."dimHSGTDomain" from "C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/dimHSGTDomain.txt" WITH DELIMITER "," CSV HEADER QUOTE ''''

select * from "School"."dimGradeType"

CREATE TABLE "School"."dimPeriod"(
	"PeriodID" integer NOT NULL,
	"Period" character(20) NULL,
 CONSTRAINT "dimPeriod_PK" PRIMARY KEY ("PeriodID")) 

 copy "School"."dimPeriod" from 'C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/dimPeriod.txt' WITH DELIMITER ',' CSV HEADER QUOTE ''''

select * from "School"."dimGradeType"

 CREATE TABLE "School"."dimSpecialProgram"(
	"ProgramID" integer NOT NULL,
	"ProgramCode" character(10) NULL,
	"Program" character(100) NULL,
 CONSTRAINT "dimSpecialProgram_PK" PRIMARY KEY ("ProgramID")) 

   copy "School"."dimSpecialProgram" from 'C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/dimSpecialProgram.txt' WITH DELIMITER ',' CSV HEADER QUOTE ''''

select * from "School"."dimGradeType"

 CREATE TABLE "School"."dimStaff"(
	"StaffID" integer NOT NULL,
	"StaffTypeID" integer NULL,
	"PrimaryFacilityID" integer NULL,
	"StaffFirstName" character(50) NULL,
	"StaffLastName" character(50) NULL,
	"StaffName" character(100) NULL,
	"StaffADUsername" character(50) NULL,
	"GradeLevel" character(50) NULL,
 CONSTRAINT "dimStaff_PK" PRIMARY KEY ("StaffID"))

 copy "School"."dimStaff" from 'C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/dimStaff.txt' WITH DELIMITER ',' CSV HEADER QUOTE ''''

 CREATE TABLE "School"."dimStandardizedTestDomain"(
	"StandardizedTestDomainID" integer NOT NULL,
	"StandardizedTestDomain" character(50) NULL,
	"SubjectID" integer NULL,
 CONSTRAINT "dimStandardizedTestDomain_PK" PRIMARY KEY ("StandardizedTestDomainID")) 

 copy "School"."dimStandardizedTestDomain" from 'C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/dimStandardizedTestDomain.txt' WITH DELIMITER ',' CSV HEADER QUOTE ''''

 CREATE TABLE "School"."dimStudent"(
	"StudentID" integer NOT NULL,
	"SISStudentID" integer NULL,
	"StudentName" character(100) NULL,
	"StudentFirstName" character(50) NULL,
	"StudentLastName" character(50) NULL,
	"BirthDate" date NULL,
	"Gender" character(10) NULL,
	"EthnicityCode" character(5) NULL,
	"Ethnicity" character(50) NULL,
	"ESOL" character(5) NULL,
	"FreeReducedLunch" character(10) NULL,
	"HomeroomStaffID" integer NULL,
	"FacilityID" integer NULL,
	"Grade" character(5) NULL,
	"AssignmentID" integer NULL,
	"EnrollmentCodeID" integer NULL,
	"EnrollmentDate" date NULL,
	"WithdrawalCodeID" integer NULL,
	"WithdrawalDate" date NULL,
	"EffectiveDate" date NOT NULL,
	"EndDate" date NOT NULL,
	"StudentMiddleName" character(50) NULL,
	"StudentNameSuffix" character(50) NULL,
	"StudentPhone" character(50) NULL,
	"StudentSSN" character(50) NULL,
	"Address1" character(50) NULL,
	"City" character(50) NULL,
	"State" character(50) NULL,
	"Zip" character(50) NULL,
	"AdultEmploymentStatus" character(20) NULL,
	"AdultLEP" character(20) NULL,
	"AttendancePeriodID" integer NULL,
	"AudiologyServiceEarlyIntervention" character(50) NULL,
	"BirthCountry" character(50) NULL,
	"BirthPlace" character(50) NULL,
	"BirthState" character(50) NULL,
	"CitizenshipStatus" character(20) NULL,
	"EarlyAdmission" character(50) NULL,
	"EconomicDisadvantage" character(50) NULL,
	"ELLCode" character(20) NULL,
	"ELLCodeDesc" character(50) NULL,
	"FederalImpactCode" character(20) NULL,
	"FederalImpactCodeDesc" character(50) NULL,
	"GiftedStudent" character(50) NULL,
	"GraduationPlanYear" character(10) NULL,
	"GraduationOption" character(50) NULL,
	"HomeLanguage" character(50) NULL,
	"HomelessStudent" character(50) NULL,
	"HomelessUnaccompaniedYouth" character(50) NULL,
	"Homeroom" character(50) NULL,
	"HomeSchoolTaught" character(50) NULL,
	"HouseholdNumber" character(10) NULL,
	"IncarceratedStudent" character(50) NULL,
	"KGReadiness" character(50) NULL,
	"MaritalStatus" character(10) NULL,
	"MedicaidSpecServices" character(50) NULL,
	"MedicalServEarlyIntervention" character(50) NULL,
	"MigrantStatus" character(50) NULL,
	"MissingChild" character(50) NULL,
	"NationalAchieveScholar" character(10) NULL,
	"NationalHispanicScholar" character(10) NULL,
	"NationalMeritScholar" character(10) NULL,
	"EarlyIntervention" character(50) NULL,
	"EarlyExceptionalities" character(50) NULL,
	"OutOfDistrict" character(10) NULL,
	"Exceptionality" character(50) NULL,
	"PrimaryLanguage" character(50) NULL,
	"ResidentStatus" character(10) NULL,
	"SecondLanguage" character(50) NULL,
	"RefID" integer NULL,
	"oldSID" integer NULL,
	"SchoolYear" integer NULL,
	"WithdrawalFlag" integer NULL,
 CONSTRAINT "dimStudent_PK" PRIMARY KEY ("StudentID")) 

  copy "School"."dimStudent" from 'C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/dimStudent.txt' WITH DELIMITER ',' CSV HEADER QUOTE ''''

CREATE TABLE "School"."dimSubject"(
	"SubjectID" integer NOT NULL,
	"SubjectCode" character(50) NULL,
	"SubjectName" character(200) NULL,
 CONSTRAINT "dimSubject_PK" PRIMARY KEY ("SubjectID")) 

 copy "School"."dimSubject" from 'C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/dimSubject.txt' WITH DELIMITER ',' CSV HEADER QUOTE ''''

 CREATE TABLE "School"."dimWithdrawalCode"(
	"WithdrawalCodeID" integer NOT NULL,
	"WithdrawalCode" character(5) NULL,
	"WithdrawalCodeDesc" character(50) NULL,
 CONSTRAINT "dimWithdrawalCode_PK" PRIMARY KEY ("WithdrawalCodeID")) 

  copy "School"."dimWithdrawalCode" from 'C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/dimWithdrawalCode.txt' WITH DELIMITER ',' CSV HEADER QUOTE ''''

 CREATE TABLE "School"."factAbsence"(
	"AbsenceID" integer NOT NULL,
	"AbsenceDate" date NULL,
	"StudentID" integer NULL,
	"SISStudentID" integer NULL,
	"AbsenceCodeID" integer NULL,
	"AbsenceCount" integer NULL,
	"FacilityID" integer NULL,
	"StaffID" integer NULL,
 CONSTRAINT "factAbsence_PK" PRIMARY KEY ("AbsenceID")) 

   copy "School"."factAbsence" from 'C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/factAbsence.txt' WITH DELIMITER ',' CSV HEADER QUOTE ''''


 CREATE TABLE "School"."factDiscipline"(
	"DisciplineID" integer  NOT NULL,
	"StudentID" integer NULL,
	"SISStudentID" integer NULL,
	"EventFacilityID" integer NULL,
	"DisciplineEventCodeID" integer NULL,
	"EventDate" date NULL,
	"EventReportingStaffID" integer NULL,
	"DisciplineActionCodeID" integer NULL,
	"ActionDate" date NULL,
	"ActionStaffID" integer NULL,
	"ResolutionDate" date NULL,
 CONSTRAINT "factDiscipline_PK" PRIMARY KEY ("DisciplineID")) 

    copy "School"."factDiscipline" from 'C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/factDiscipline.txt' WITH DELIMITER ',' CSV HEADER QUOTE ''''


 CREATE TABLE "School"."factHSGTDomainScore"(
	"HSGTDomainScoreID" integer NOT NULL,
	"HSGTScoreID" integer NOT NULL,
	"HSGTDomainID" integer NOT NULL,
	"DomainRawScore" numeric(10, 2) NULL,
	"PossibleDomainRawScore" numeric(10, 2) NULL,
 CONSTRAINT "PK_factHSGTDomainScore" PRIMARY KEY ("HSGTDomainScoreID")) 

     copy "School"."factHSGTDomainScore" from 'C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/factHSGTDomainScore.txt' WITH DELIMITER ',' CSV HEADER QUOTE ''''
 
CREATE TABLE "School"."factHSGTScore"(
	"HSGTScoreID" integer NOT NULL,
	"StudentID" integer NOT NULL,
	"SISStudentID" integer NULL,
	"TestDate" date NULL,
	"SubjectID" integer NULL,
	"RawScore" numeric(10, 2) NULL,
	"PossibleRawScore" numeric(10, 2) NULL,
	"ScaledScore" numeric(10, 2) NULL,
	"PossibleScaledScore" numeric(10, 2) NULL,
	"PassFail" character(50) NULL,
	"TimesTested" integer NULL,
	"LatestTest" integer NULL,
 CONSTRAINT "PK_factHSGTScore" PRIMARY KEY ("HSGTScoreID")) 

 copy "School"."factHSGTScore" from 'C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/factHSGTScore.txt' WITH DELIMITER ',' CSV HEADER QUOTE ''''


 CREATE TABLE "School"."factSchoolKPI"(
	"SchoolKPIID" integer NOT NULL,
	"EffectiveDate" date NOT NULL,
	"FacilityID" integer NOT NULL,
	"TargetStudentAttendancePct" integer NOT NULL,
	"TargetStandardizedTestScaledScore" integer NOT NULL,
	"TargetEnrollmentCount" integer NOT NULL,
 CONSTRAINT "PK_factSchoolKPI" PRIMARY KEY ("SchoolKPIID"))

  copy "School"."factSchoolKPI" from 'C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/factSchoolKPI.txt' WITH DELIMITER ',' CSV HEADER QUOTE ''''


 CREATE TABLE "School"."factSpecialProgram"(
	"SpecialProgramID" integer NOT NULL,
	"ProgramID" integer NULL,
	"StudentID" integer NULL,
	"SISStudentID" integer NULL,
	"EntryDate" date NULL,
 CONSTRAINT "factSpecialProgram_PK" PRIMARY KEY ("SpecialProgramID")) 

   copy "School"."factSpecialProgram" from 'C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/factSpecialProgram.txt' WITH DELIMITER ',' CSV HEADER QUOTE ''''


 CREATE TABLE "School"."factStandardizedTestDomainScore"(
	"StandardizedTestDomainScoreID" integer NOT NULL,
	"StandardizedTestScoreID" integer NULL,
	"StandardizedTestDomainID" integer NULL,
	"RawScore" numeric(10, 2) NULL,
	"PossibleRawScore" numeric(10, 2) NULL,
 CONSTRAINT "factStandardizedTestDomainScore_PK" PRIMARY KEY ("StandardizedTestDomainScoreID")) 

    copy "School"."factStandardizedTestDomainScore" from 'C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/factStandardizedTestDomainScore.txt' WITH DELIMITER ',' CSV HEADER QUOTE ''''


 CREATE TABLE "School"."factStandardizedTestScore"(
	"StandardizedTestScoreID" integer NOT NULL,
	"StudentID" integer NULL,
	"SISStudentID" integer NULL,
	"TestDate" date NULL,
	"SubjectID" integer NULL,
	"RawScore" numeric(10, 2) NULL,
	"PossibleRawScore" numeric(10, 2) NULL,
	"ScaledScore" numeric(10, 2) NULL,
	"PossibleScaledScore" numeric(10, 2) NULL,
	"PerformanceLevel" numeric(10, 2) NULL,
	"Lexile" character(50) NULL,
	"LatestTest" integer NULL,
 CONSTRAINT "factStandardizedTestScore_PK" PRIMARY KEY ("StandardizedTestScoreID")) 

 copy "School"."factStandardizedTestScore" from 'C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/factStandardizedTestScore.txt' WITH DELIMITER ',' CSV HEADER QUOTE ''''

 CREATE TABLE "School"."factStudentCourse"(
	"StudentCourseID" integer NOT NULL,
	"StudentID" integer NULL,
	"SISStudentID" integer NULL,
	"CourseID" integer NULL,
	"StaffID" integer NULL,
	"FacilityID" integer NULL,
	"PeriodID" integer NULL,
	"CourseDate" date NULL,
	"EnrollmentDate" date NULL,
	"EnrollmentCodeID" integer NULL,
	"WithdrawalDate" date NULL,
	"WithdrawalCodeID" integer NULL,
	"GradeLevel" character(10) NULL,
 CONSTRAINT "factStudentCourse_PK" PRIMARY KEY ("StudentCourseID"))

  copy "School"."factStudentCourse" from 'C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/factStudentCourse.txt' WITH DELIMITER ',' CSV HEADER QUOTE ''''
 

 CREATE TABLE "School"."factStudentCourseGrade"(
	"StudentCourseGradeID" integer  NOT NULL,
	"StudentCourseID" integer NULL,
	"GradeTypeID" integer NULL,
	"CreditAttempted" numeric(10, 2) NULL,
	"CreditGiven" numeric(10, 2) NULL,
	"NumericGrade" numeric(10, 2) NULL,
	"AlphaGrade" character(5) NULL,
	"AlphaNumericGrade" numeric(10, 2) NULL,
 CONSTRAINT "factStudentCourseGrade_PK" PRIMARY KEY ("StudentCourseGradeID")) 

   copy "School"."factStudentCourseGrade" from 'C:/Users/MASTERCHIEF/Downloads/Schools_K12_DW_falt_files/factStudentCourseGrade.txt' WITH DELIMITER ',' CSV HEADER QUOTE ''''


 