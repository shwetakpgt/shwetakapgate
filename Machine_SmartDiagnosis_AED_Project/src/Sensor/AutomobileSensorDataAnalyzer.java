/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sensor;

/**
 *
 * @author shweta
 */
public class AutomobileSensorDataAnalyzer {
  
   public SensorStatus analyze(AutomobileSensorData sensorData){
       if((sensorData.getEmission()>=3.0)||sensorData.getHydraulicPressure()>=2.0|| sensorData.getBrakeLines()==3){
       return SensorStatus.Critical;
       }
       else if((sensorData.getEmission()>=2.0 && sensorData.getEmission()<3.0)||(sensorData.getHydraulicPressure()<2.0&& sensorData.getHydraulicPressure()>=1.0)||sensorData.getBrakeLines()==2)
               {
       return SensorStatus.TroubleShoot;
       }
       return SensorStatus.Normal;
       }
   }
