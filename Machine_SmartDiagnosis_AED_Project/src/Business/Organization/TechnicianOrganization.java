/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.HospitalAdminRole;
import Business.Role.Role;
import Business.Role.TechnicianRole;
import java.util.ArrayList;


public class TechnicianOrganization extends Organization{

    public TechnicianOrganization() {
        super(Type.Technician.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new TechnicianRole());
        return roles;
    }
     
}
