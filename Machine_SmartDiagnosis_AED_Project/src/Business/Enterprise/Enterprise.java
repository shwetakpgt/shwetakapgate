/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Organization.Organization;
import Business.Organization.OrganizationDirectory;
import Sensor.MachineryDetectionDirectory;
import Sensor.SensorTroubleShootDirectory;

/**
 *
 * @author raunak
 */
public abstract class Enterprise extends Organization{

    private EnterpriseType enterpriseType;
    private OrganizationDirectory organizationDirectory;
    private MachineryDetectionDirectory machineryDetectionDirectory;
    private SensorTroubleShootDirectory sensorTroubleShootDirectory;
    
    public Enterprise(String name, EnterpriseType type) {
        super(name);
        this.enterpriseType = type;
        organizationDirectory = new OrganizationDirectory();
        machineryDetectionDirectory=new MachineryDetectionDirectory();
        sensorTroubleShootDirectory= new SensorTroubleShootDirectory();
    }

    public SensorTroubleShootDirectory getSensorTroubleShootDirectory() {
        return sensorTroubleShootDirectory;
    }

    public void setSensorTroubleShootDirectory(SensorTroubleShootDirectory sensorTroubleShootDirectory) {
        this.sensorTroubleShootDirectory = sensorTroubleShootDirectory;
    }

    public MachineryDetectionDirectory getMachineryDetectionDirectory() {
        return machineryDetectionDirectory;
    }

    public void setMachineryDetectionDirectory(MachineryDetectionDirectory machineryDetectionDirectory) {
        this.machineryDetectionDirectory = machineryDetectionDirectory;
    }
    
    public enum EnterpriseType{
        Hospital("Hospital Equipment Industry"),
        AutoMobile ("Automobile Industry");
        
        private String value;

        private EnterpriseType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }

    public OrganizationDirectory getOrganizationDirectory() {
        return organizationDirectory;
    }

}
