/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sensor;

/**
 *
 * @author shweta
 */
public enum SensorStatus {

    Critical("Critical"),
    Normal("Normal"),
    TroubleShoot("TroubleShoot");
    private String value;

    private SensorStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
