/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.AutomobileAdminRole;
import Business.Role.Role;
import Business.Role.SpecialistRole;
import java.util.ArrayList;

/**
 *
 * @author shweta
 */
public class SpecialistOrganization extends Organization {
 
    public SpecialistOrganization() {
        super(Organization.Type.Specialist.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new SpecialistRole());
        return roles;
    }
     
}
