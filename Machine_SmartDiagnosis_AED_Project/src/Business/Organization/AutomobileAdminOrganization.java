/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.AutomobileAdminRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author shweta
 */
public class AutomobileAdminOrganization extends Organization {
 
    public AutomobileAdminOrganization() {
        super(Organization.Type.AutomobileAdminOrganization.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new AutomobileAdminRole());
        return roles;
    }
     
}
