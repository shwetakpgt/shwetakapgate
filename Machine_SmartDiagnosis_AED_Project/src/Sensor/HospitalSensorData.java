/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sensor;



/**
 *
 * @author shweta
 */
public class HospitalSensorData {
    private boolean motorSatus;
    private String CTScanners;
    private String Vantilators;

    public boolean isMotorSatus() {
        return motorSatus;
    }

    public void setMotorSatus(boolean motorSatus) {
        this.motorSatus = motorSatus;
    }

    public String getCTScanners() {
        return CTScanners;
    }

    public void setCTScanners(String CTScanners) {
        this.CTScanners = CTScanners;
    }

    public String getVantilators() {
        return Vantilators;
    }

    public void setVantilators(String Vantilators) {
        this.Vantilators = Vantilators;
    }
    
}
